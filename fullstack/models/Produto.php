<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "produto".
 *
 * @property int $id
 * @property string $nome
 * @property string $codigoBarras
 * @property float $valorUnitario
 *
 * @property Pedido[] $pedidos
 */
class Produto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'produto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome', 'codigoBarras', 'valorUnitario'], 'required'],
            [['valorUnitario'], 'number'],
            [['nome'], 'string', 'max' => 100],
            [['codigoBarras'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'codigoBarras' => 'Código de Barras',
            'valorUnitario' => 'Valor Unitário',
        ];
    }

    /**
     * Gets query for [[Pedidos]].
     *
     * @return \yii\db\ActiveQuery|PedidoQuery
     */
    public function getPedidos()
    {
        return $this->hasMany(Pedido::className(), ['produto_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ProdutoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProdutoQuery(get_called_class());
    }
}

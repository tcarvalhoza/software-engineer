<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "pedido".
 *
 * @property int $id
 * @property string $dataPedido
 * @property int $cliente_id
 * @property int $produto_id
 * @property string $status
 * @property int $quantidade 
 *
 * @property Cliente $cliente
 * @property Produto $produto
 */
class Pedido extends \yii\db\ActiveRecord
{
    const STATUS_EM_ABERTO = 'Em Aberto';
    const STATUS_PAGO      = 'Pago';
    const STATUS_CANCELADO = 'Cancelado';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pedido';
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cliente_id', 'produto_id', 'status', 'quantidade'], 'required'],
            [['dataPedido'], 'safe'],
            [['cliente_id', 'produto_id', 'quantidade'], 'integer'],
            [['status'], 'string', 'max' => 20],
            [['cliente_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['cliente_id' => 'id']],
            [['produto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Produto::className(), 'targetAttribute' => ['produto_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dataPedido' => 'Data do Pedido',
            'cliente_id' => 'Cliente',
            'produto_id' => 'Produto',
            'status' => 'Status',
            'quantidade' => 'Quantidade', 
        ];
    }

    /**
     * Gets query for [[Cliente]].
     *
     * @return \yii\db\ActiveQuery|ClienteQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Cliente::className(), ['id' => 'cliente_id']);
    }

    /**
     * Gets query for [[Produto]].
     *
     * @return \yii\db\ActiveQuery|ProdutoQuery
     */
    public function getProduto()
    {
        return $this->hasOne(Produto::className(), ['id' => 'produto_id']);
    }

    /**
     * {@inheritdoc}
     * @return PedidoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PedidoQuery(get_called_class());
    }
    
        
    /**
     * Retorna um array com os Status do Pedido
     * 
     * @return array 
     */
    public function listarStatusPedido() 
    {
        $items = [
            self::STATUS_EM_ABERTO => self::STATUS_EM_ABERTO,
            self::STATUS_PAGO => self::STATUS_PAGO,
            self::STATUS_CANCELADO => self::STATUS_CANCELADO,
        ];
        
        return $items;
    }
    /**
     * Retorna um array com os Status do Pedido
     * 
     * @return array 
     */
    public function listarClientes() 
    {
        $items = [
            self::STATUS_EM_ABERTO => self::STATUS_EM_ABERTO,
            self::STATUS_PAGO => self::STATUS_PAGO,
            self::STATUS_CANCELADO => self::STATUS_CANCELADO,
        ];
        
        return $items;
    }
    
    /* Preenche os atributos taxapagamento e taxaconversao com seus rescpectivos
     * calculos antes do model ser salvo no banco de dados.
     * 
     * @return float 
     */
    public function beforeSave($insert) {

//        $this->dataPedido = new yii\db\Expression('NOW()');
        $this->dataPedido = date('Y-m-d H:i:s');

        return parent::beforeSave($insert);
    }
}

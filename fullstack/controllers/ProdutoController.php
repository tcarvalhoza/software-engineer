<?php

namespace app\controllers;

use app\models\Produto;
use app\models\ProdutoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProdutoController implementa as ações do CRUD do model Produto.
 */
class ProdutoController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Exibe todos os produtos cadastrados
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ProdutoSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Exibe as informações de um produto
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException Se não encontrou nenhum produto
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Cadastra um novo Produto
     * Se o cadastro ocorrer normalmente, exibe os detalhes na view
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Produto();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->validate()) {
                if($model->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Altera um produto existente
     * Se o cadastro ocorrer normalmente, exibe os detalhes na view.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException Se não encontrou nenhum produto
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post())) {
            if ($model->validate()) {
                if($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deleta um produto existente
     * Se a exclusão ocorrer normalmente, exibe a pagina com lista de clientes.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException Se não encontrou nenhum produto
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Busca um produto através de seu ID (primary key)
     * Se não encontrou nenhum cliente, retorna erro 404.
     * @param int $id ID
     * @return Produto the loaded model
     * @throws NotFoundHttpException Se não encontrou nenhum produto
     */
    protected function findModel($id)
    {
        if (($model = Produto::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('A página solicitada não existe.');
    }
}

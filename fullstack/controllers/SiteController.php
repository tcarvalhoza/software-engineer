<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\Url;

class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Exibe a homepage
     *
     * @return string
     */
    public function actionIndex()
    {
        $urlCliente = Url::toRoute(['cliente/index']);
        $urlProduto = Url::toRoute(['produto/index']);
        $urlPedido = Url::toRoute(['pedido/index']);
        
        return $this->render('index', 
        [
            'urlCliente'=>$urlCliente,
            'urlProduto'=>$urlProduto,
            'urlPedido'=>$urlPedido
        ]);
    }

}

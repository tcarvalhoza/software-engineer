<?php

namespace app\controllers;

use app\models\Pedido;
use app\models\PedidoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\Cliente;
use app\models\Produto;

/**
 * PedidoController implementa as ações do CRUD do model Pedido.
 */
class PedidoController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Exibe todos os pedidos cadastrados
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PedidoSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listaStatus' => (new Pedido())->listarStatusPedido(),
            'listaClientes' => $this->listarClientes(),
            'listaProdutos' => $this->listarProdutos()
        ]);
    }

    /**
     * Exibe as informações de um pedido
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException Se não encontrou nenhum pedido
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Cadastra um novo pedido
     * Se o cadastro ocorrer normalmente, exibe os detalhes na view
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Pedido();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->validate()) {
                if($model->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'listaStatus' => $model->listarStatusPedido(),
            'listaClientes' => $this->listarClientes(),
            'listaProdutos' => $this->listarProdutos()
        ]);
    }

    /**
     * Altera um pedido existente
     * Se o cadastro ocorrer normalmente, exibe os detalhes na view.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException Se não encontrou nenhum pedido
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post())) {
            if ($model->validate()) {
                if($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'listaStatus' => $model->listarStatusPedido(),
            'listaClientes' => $this->listarClientes(),
            'listaProdutos' => $this->listarProdutos()
        ]);
    }

    /**
     * Deleta um pedido existente
     * Se a exclusão ocorrer normalmente, exibe a pagina com lista de pedidos.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException Se não encontrou nenhum pedido
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Busca um pedido através de seu ID (primary key)
     * Se não encontrou nenhum pedido, retorna erro 404.
     * @param int $id ID
     * @return Pedido the loaded model
     * @throws NotFoundHttpException Se não encontrou nenhum pedido
     */
    protected function findModel($id)
    {
        if (($model = Pedido::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('A página solicitada não existe.');
    }
    
    /**
     * Retorna um Array de Clientes para preencher um dropdownlist
     * 
     * @return Array
     */
    private function listarClientes() {
        
        return ArrayHelper::map(Cliente::find()->all(), 'id', 'nome');
    }
    
    /**
     * Retorna um Array de Produtos para preencher um dropdownlist
     * 
     * @return Array
     */
    private function listarProdutos() {
        
        return ArrayHelper::map(Produto::find()->all(), 'id', 'nome');
    }
}

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PedidoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pedidos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedido-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Cadastrar Pedido', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>'cliente_id',
                'label'=>'Cliente',
                'value'=>function($data){
                    return $data->cliente->nome;
                },
                'filter' =>$listaClientes
            ],
            [
                'attribute'=>'produto_id',
                'label'=>'Produto',
                'value'=>function($data){
                    return $data->produto->nome;
                },
                'filter' =>$listaProdutos
            ],
            'status',
            'quantidade', 
            [
                'attribute'=>'dataPedido',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDateTime(strtotime($model->dataPedido), 'php: H:i:s d/m/Y');
                },
                'filter'=>false
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pedido */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pedidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pedido-view">

    <h1>Exibindo Pedido: <?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Deletar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Tem certeza que deseja deletar esse item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute'=> 'cliente.nome',
                'label'=>'Cliente',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->cliente->nome,['cliente/view?id='.$data->cliente->id]);
                },
            ],
            [
                'attribute'=> 'produto.nome',
                'label'=>'Produto',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->produto->nome,['produto/view?id='.$data->produto->id]);
                },
            ],
            'status',
            'quantidade', 
            [
                'attribute'=>'dataPedido',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDateTime(strtotime($model->dataPedido), 'php: H:i:s d/m/Y');
                },
            ],
        ],
    ]) ?>

</div>

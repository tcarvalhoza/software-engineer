<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pedido */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pedido-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->errorSummary($model) ?>
    
    <?= $form->field($model, 'cliente_id')->dropDownList($listaClientes, ['prompt'=>' -- Selecione --',]) ?>

    <?= $form->field($model, 'produto_id')->dropDownList($listaProdutos, ['prompt'=>' -- Selecione --',]) ?>

    <?= $form->field($model, 'status')->dropDownList($listaStatus, ['prompt'=>' -- Selecione --',]) ?>
    
    <?= $form->field($model, 'quantidade')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

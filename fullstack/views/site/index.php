<?php

/* @var $this yii\web\View */

$this->title = 'Fullstack';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Bem vindo!</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Clientes</h2>
                <a class="btn btn-lg btn-info" href="<?= $urlCliente ?>">Lista de Clientes</a>
            </div>
            <div class="col-lg-4">
                <h2>Produtos</h2>
                <a class="btn btn-lg btn-info" href="<?= $urlProduto ?>">Lista de Produtos</a>
            </div>
            <div class="col-lg-4">
                <h2>Pedidos</h2>
                <a class="btn btn-lg btn-info" href="<?= $urlPedido ?>">Lista de Pedidos</a>
            </div>
        </div>

    </div>
</div>

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%pedido}}`.
 */
class m220127_132642_create_pedido_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pedido}}', [
            'id' => $this->primaryKey(),
            'dataPedido' => $this->timestamp()->notNull(),
            'cliente_id' => $this->integer('11')->notNull(),
            'produto_id' => $this->integer('11')->notNull(),
            'status'     => $this->string('20')->notNull(),
            'quantidade' => $this->integer('11')->notNull()
        ]);
        
        /* Foi decisão minha colocar o DELETE CASCADE já que nas instruções
         * foi pedido que deveria permitir qualquer exclusão.
         * 
         * Senão eu faria uma consulta para cada item da lista e só exibiria o 
         * botão DELETAR para Clientes e Produtos que não tivessem nenhum 
         * pedido cadastrado ou daria a opção de excluir os pedidos antes.
         * 
         */
        $this->addForeignKey(
            'fk_pedido_cliente_id',
            'pedido',
            'cliente_id',
            'cliente',
            'id',
            'CASCADE'
        );
        
        $this->addForeignKey(
            'fk_pedido_produto_id',
            'pedido',
            'produto_id',
            'produto',
            'id',
            'CASCADE'
        );
        
        $this->insert('pedido',
        [
            'dataPedido' => new yii\db\Expression('NOW()'),
            'cliente_id' => 1,
            'produto_id' => 1,
            'status'     => 'Em Aberto',
            'quantidade' => 1
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%pedido}}');
    }
}

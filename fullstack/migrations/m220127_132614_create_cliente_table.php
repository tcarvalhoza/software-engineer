<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cliente}}`.
 */
class m220127_132614_create_cliente_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cliente}}', 
        [
            'id' => $this->primaryKey(),
            'nome' => $this->string('100')->notNull(),
            'cpf' => $this->char('11')->notNull(),
            'email' => $this->string('100'),
        ]);
        
        $this->insert('cliente',
        [
            'nome' =>'Terence',
            'cpf' => '09919964700',
            'email' => 'tcarvalhoza@gmail.com'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cliente}}');
    }
}

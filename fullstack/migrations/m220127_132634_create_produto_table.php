<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%produto}}`.
 */
class m220127_132634_create_produto_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%produto}}', [
            'id' => $this->primaryKey(),
            'nome' => $this->string('100')->notNull(),
            'codigoBarras' => $this->string('20')->notNull(),
            'valorUnitario' => $this->money()->notNull(),
        ]);
        
        $this->insert('produto',
        [
            'nome' =>'Mouse',
            'codigoBarras' => '102030',
            'valorUnitario' => 50
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%produto}}');
    }
}

### Instalação do ambiente Docker 

## 1 - Acessar o diretório software-enginer pelo terminal
## 2 - Executar o comando "docker-composer up --build"

### Instalação do Projeto
## 1 - Acessar o diretório software-enginer/fullstack pelo terminal
## 2 - Executar o comando "php yii migrate" e responder "yes"
## 3 - Após a execução das migrates, o sistema estará pronto e acessível.
## 4 - Poderá ser acessado através do link: http://localhost:8001/web/ ou equivalente dependendo do seu ambiente docker.

### Tecnologias Utilizadas
## Yii2 Framework
## PHP 7.4
## MySQL 8.0.28